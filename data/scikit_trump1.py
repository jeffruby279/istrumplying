from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, cross_val_score
from sklearn import svm
from sklearn.neural_network import MLPClassifier

import pickle
import random
import joblib
import re
from functools import *
import numpy as np

# master = joblib.load('master.joblib')

# import feedparser as rss
# LISTOFFEEDS = 'twbrss.txt'
# import time
# masterfeed = []
# i=1
# with open(LISTOFFEEDS) as f:
#     for line in f:
#         p = rss.parse(line)
#         masterfeed = set(masterfeed).union(p['entries'])
#         print("another file ", i)
#         i = i + 1
#         time.sleep(random.randint(3,10))
# master = list(masterfeed) #still duplicates
# joblib.dump(master, 'master.joblib')

#still duplicates in master

import joblib
trump_train = joblib.load('trump_train.joblib')

def categorize_entry(entry):
    if ': True' in entry.summary:
        entry.category = 0
    elif 'Mostly True' in entry.summary:
        entry.category = 1
    elif 'Half-True' in entry.summary:
        entry.category = 2
    elif 'Mostly False' in entry.summary:
        entry.category = 3
    elif ': False' in entry.summary:
        entry.category = 4
    elif 'Pants on Fire!' in entry.summary:
        entry.category = 5
    elif 'No Flip' in entry.summary:
        entry.category = 6
    elif 'Half Flip' in entry.summary:
        entry.category = 7
    elif 'Full Flop' in entry.summary:
        entry.category = 8

import mechanicalsoup
def get_first_tweet_quote(entry):
    browser = mechanicalsoup.StatefulBrowser()
    browser.open(entry.link)
    page = browser.get_current_page()
    first_tweet = page.find('blockquote', class_='twitter-tweet')
    browser.close()
    if first_tweet:
        return re.sub(r'— Donald J\. Trump .*', '', first_tweet.text)
    else:
        return None

from sklearn.linear_model import *
from sklearn.naive_bayes import *

random.shuffle(trump_train)

trump_data=[]
trump_target=[]
for entry in trump_train:
    trump_data.append(entry.tweet)
    trump_target.append(entry.category)
trump_data = np.asarray(trump_data)
trump_target = np.asarray(trump_target)

from sklearn.model_selection import KFold, cross_val_score
# k_fold = KFold(n_splits=3)
k_fold = KFold(n_splits=9)
# k_fold = KFold(n_splits=423)

import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import RidgeClassifierCV
trump_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', RidgeClassifierCV(alphas=[1e-3, 1e-2, 1e-1, 1])),
])
# trump_clf = Pipeline([
#     ('vect', CountVectorizer()),
#     ('tfidf', TfidfTransformer()),
#     ('clf', SGDClassifier(loss='hinge', penalty='l2',
#                           alpha=1e-3, random_state=42,
#                           max_iter=5, tol=None)),
# ])
[trump_clf.fit(trump_data[train], trump_target[train]).score(trump_data[test], trump_target[test])
 for train, test in k_fold.split(trump_target)] 

# parameters = {
#     'vect__ngram_range': [(1, 1), (1, 2)],
#     'tfidf__use_idf': (True, False),
#     'clf__alpha': (1e-2, 1e-3),
# }
# trump_gs_clf = GridSearchCV(trump_clf, parameters, cv=5, iid=False, n_jobs=-1)

# [trump_gs_clf.fit(trump_data[train], trump_target[train]).score(trump_data[test], trump_target[test])
#  for train, test in k_fold.split(trump_target)] 


# predicted = trump_gs_clf.predict(trump_test_data)
# np.mean(predicted == trump_test_target)   

# trump_train_data = []
# trump_train_target = []
# for entry in trump_train[-325:]:
#     trump_train_data.append(entry.tweet)
#     trump_train_target.append(entry.category)

# trump_test_data = []
# trump_test_target = []
# for entry in trump_train[:-325]:
#     trump_test_data.append(entry.tweet)
#     trump_test_target.append(entry.category)

# trump_clf = Pipeline([
#     ('vect', CountVectorizer()),
#     ('tfidf', TfidfTransformer()),
#     ('clf',MLPClassifier(solver='sgd', alpha=0.01,
#                     hidden_layer_sizes=(450), random_state=1)),
# ])


# trump_clf.fit(trump_train_data, trump_train_target)  
# predicted = trump_clf.predict(trump_test_data)
# np.mean(predicted == trump_test_target)   






