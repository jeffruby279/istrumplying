import flask
from flask import request, jsonify, json
# from flask_cors import CORS # azure doesnt like this package
import joblib
import requests
import sklearn

app = flask.Flask(__name__)
app.config["DEBUG"] = True
# CORS(app)

trump_clf = joblib.load('trump_clf.joblib')

# reverse chronological order
archive2019URL = "http://www.trumptwitterarchive.com/data/realdonaldtrump/2019.json"
archive2019file = "data/2019.json"  # reverse chronological order
archiveOldfile = "data/unclassified.json"  # reverse chronological order
unclassified_tweets = []

# this is not good i really should be using the twitter api
def update_tweets():
    if True: # todo dont update the latest twwets on every api call
        response = requests.get(archive2019URL)
        data = response.json()
        data = list(filter(lambda tweet: not tweet['is_retweet'], data))
        with open(archive2019file, 'wt') as f:
            f.write(json.dumps(data))
        with open(archiveOldfile) as f:
            unclassified_tweets = json.load(f)
        data.extend(unclassified_tweets)
        return data


with open(archiveOldfile) as f:
    unclassified_tweets = json.load(f)
unclassified_tweets = list(
    filter(lambda tweet: not tweet['is_retweet'], unclassified_tweets))
unclassified_tweets = update_tweets()


def categorize_tweets(tweets):
    for tweet in tweets:
        tweet['category'] = trump_clf.predict([tweet['text']])[0].item()


# @app.route("/")
# def hello():
#     return "Hello World!"


# @app.route('/api/TrumpTextClassify', methods=['GET'])
# def TrumpTextClassify():
#     if 'clf' in request.args:
#         clf = request.args['clf']
#     else:
#         return "Error: No text provided."

#     results = trump_clf.predict(clf).tolist()

#     return jsonify(results)

@app.route('/api/mostrecent')
def most_recent():
    update_tweets()
    return recent(20)


@app.route('/api/recent/<int:numResults>')
def recent(numResults):
    if request.args.get('q') is not None:
        qs = request.args.get('q').strip('"').lower().split()
        for q in qs:
            tweets = list(filter(
                lambda tweet: q in tweet['text'].lower(), unclassified_tweets))[:numResults]
    else:
        tweets = unclassified_tweets[:numResults]
    categorize_tweets(tweets)
    return jsonify(tweets)


@app.route('/api/search')
def search():
    qs = request.args.get('q').strip('"').lower().split()
    tweets = []
    for q in qs:
        tweets.extend(
            list(filter(lambda tweet: q in tweet['text'].lower(), unclassified_tweets)))
    categorize_tweets(tweets)
    return jsonify(tweets)

# uncomment the following line to run the dev server locally
#app.run()
